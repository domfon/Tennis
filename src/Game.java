/**
 * Created by Dom on 15/07/2017.
 */
public class Game {
    private Scoreboard scoreBoard;

    public Game(Scoreboard scoreBoard) {
        this.scoreBoard = scoreBoard;
    }

    public void score(String points) {
        for (String point : points.split("")) {
            if (point.equals("1")){
                scoreBoard.player1Scores();
            }
            else {
                scoreBoard.player2Scores();
            }
            scoreBoard.report();
        }
    }
}
