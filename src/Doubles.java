import java.io.PrintStream;

/**
 * Created by Dom on 15/07/2017.
 */
class Scoreboard {
    private int player1Name;
    private int player2Name;
    private int player3Name;
    private int player4Name;
    private Score player1Set;
    private Score player3Set;
    private Score player1Games;
    private Score player3Games;
    private Score player1Score;
    private Score player3Score;
    private PrintStream printStream;

    public Scoreboard(PrintStream printStream, int player1Name, int player2Name, int player3Name, int player4Name, Score player1Score, Score player2Score, Score player3Score, Score player4Score,
                      Score player1Set, Score player2Set, Score player3Set, Score player4Set, Score player1Games, Score player2Games, Score player3Games, Score player4Games) {

        this.printStream = printStream;
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        this.player3Name = player3Name;
        this.player4Name = player4Name;

        this.player1Set = player1Set;
        this.player3Set = player3Set;

        this.player1Games = player1Games;
        this.player3Games = player3Games;

        this.player1Score = player1Score;
        this.player3Score = player3Score;
    }

    //Constructor
    public Scoreboard(PrintStream out, Name player1Name, Name player3Name, Set player1Set, Set player3Set, Game player1Games Game player3Games, Score score,  Score score1, Score score3) {
    }

    public void player1Scores() {
        player1Score.winsPoint();
    }
    public void player3Scores() {
        player3Score.winsPoint();
    }

    //    This section will print out the inputted player names, the set of the game, the game currently anad the score.
    public void report() {
        printStream.println(player1Name + "/" + player2Name + "/" + player1Set + "/" + player1Games  + "/" + player1Score);
        printStream.println(player3Name + "/" +  player4Name + "/" + player3Set + "/" + player3Games  + "/" + player3Score);
    }

    public static void main(String[] args) {
        new Game(new Scoreboard(System.out, new Score(0), new Score(0))).score("11221");
    }
}
}

