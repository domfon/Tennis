import java.io.PrintStream;

/**
 * Created by Dom on 16/07/2017.
 */
class Scoreboard {
    private int player1Name;
    private int player2Name;
    private Score player1Set;
    private Score player2Set;
    private Score player1Games;
    private Score player2Games;
    private Score player1Score;
    private Score player2Score;
    private PrintStream printStream;

    public Scoreboard(PrintStream printStream, int player1Name, int player2Name, Score player1Score, Score player2Score, Score player1Set, Score player2Set, Score player1Games, Score player2Games) {

        this.printStream = printStream;
        this.player1Name = player1Name;
        this.player2Name = player2Name;

        this.player1Set = player1Set;
        this.player2Set = player2Set;

        this.player1Games = player1Games;
        this.player2Games = player2Games;

        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    //Constructor
    public Scoreboard(PrintStream out, Name player1Name, Name player2Name, Set player1Set, Set player2Set, Game player1Games, Game player2Games, Score score, Score score1, Score score2) {
    }

    public void player1Scores() {
        player1Score.winsPoint();
    }

    public void player2Scores() {
        player2Score.winsPoint();
    }

    //    This section will print out the inputted player names, the set of the game, the game currently anad the score.
    public void report() {
        printStream.println(player1Name + "/" + player1Set + "/" + player1Games + "/" + player1Score);
        printStream.println(player2Name + "/" + player2Set + "/" + player2Games + "/" + player2Score);
    }
}

